<?php

function _signup_options_node_form($node = NULL) {
  $num_options = variable_get('signup_options_max_options', 3);
  $options = array(
    '#type' => 'fieldset',
    '#title' => 'Options',
    '#attributes' => array(
      'class' => 'signup-options'
    ),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#weight' => 3,
    '#Description' => t('Offer registration options.'),
  );

  for ( $i = 1; $i <= $num_options; $i++) {
    $options[$i] = array(
      '#type' => 'fieldset',
      '#title' => '',
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
      '#attributes' => array(
        'class' => 'signup-options-option',
      ),
      'signup_option' . $i => array(
        '#type' => 'textfield',
        '#title' => 'Title',
        '#maxlength' => 30,
        '#description' => t('Leave blank to disable'),
        '#default_value' => $node->signup_options["$i"]['title'],
        '#size' => 15,
      ),
      'signup_slot' . $i => array(
        '#type' => 'textfield',
        '#title' => 'Slots',
        '#description' => t('Max number of people who can register for this option.'),
        '#maxlength' => 3,
        '#default_value' => $node->signup_options["$i"]['slots'],
        '#size' => 3,
      ),
    );
  }
<<<<<<< HEAD
  
=======
>>>>>>> dev
  return $options;
}

function _signup_options_signup_form($form_state, $node) {
  global $user;
  $form = array(
    'signup_options' => array(
      '#type' => 'radios',
    ),
    'nid' => array(
      '#type' => 'value',
      '#value' => $node->nid,
    ),
    '#prefix' => '<div class="signup-options-list">',
    '#suffix' => "</div><!-- end signup-options-list -->",
    'submit' => array(
      '#type' => 'submit',
      '#value' => t('Submit'),
      '#attributes' => array('class' => 'signup-options-signup-submit'),
    ), 
  );

   // Get the list of signed up users
  $query = db_query("SELECT uid, signup_time, oid FROM {signup_log} WHERE nid = %d", $node->nid);
  $signees = array();
  while ($row = db_fetch_array($query)) {
    $signees[$row['oid']][] = $row;
  }
 
  $num_options = variable_get('num_options', 3);
  $options = $node->signup_options;
  
  // Obtain a count of active options. Used to set the classes for the column headers.
  $count = 0;
  for ($i=1; $i <= $num_options; $i++) {
    if (isset($node->signup_options[$i]) || !empty($signees[$i])) {
      $count++;
    }
  }

  //if no option was created, use node title as default
  if (count($options) == 0) {
    $options[1] = array('title' => $node->title, 'slots' => 0);
    // count number of columns
    $count = 1;
  }

  // print columns of signed-up users
  for ($i=1; $i <= $num_options; $i++) {
    if (isset($options[$i]) || !empty($signees[$i])) {
      $op_prefix = '<div class="signup-options-column signup-options-column-' . $count . '-col signup-options-column-' . $i . '">' . "\n";
      $title = (!empty($options[$i]['title'])) ? $options[$i]['title'] : $node->title;
      $op_prefix .= "<h2>{$title}</h2>\n";
      $op_suffix = '<ul class="signup-option-' . $i . '"' . ">\n";
      $rt = 'odd';
      $signed_up = FALSE;
      foreach ($signees[$i] as $signee) {
        if ($user->uid != 0 && $user->uid == $signee['uid']) {
          $signed_up = TRUE;
        }
        $signee_user = user_load($signee['uid']);
        $op_suffix .= '<li class="' . $rt . '">';
        $op_suffix .= '<div class="signup-options-signee"> ' . theme('username', $signee_user) . '</div>';
        $op_suffix .= '<div class="signup-options-date">' . format_date($signee['signup_time'], 'small') . "</div></li>\n";
      }
      $op_suffix .= "</ul>\n</div><!-- end signup-options-column -->";
      if (!user_access('sign up for content')) {
        $form['signup_options'] = array(
          '#type' => 'markup',
          '#suffix' => $op_suffix,
          '#prefix' => $op_prefix,
          '#value' => ' ',
        );
      }
      elseif ($signed_up) {
        $form['signup_options'][$i] = array(
          '#type' => 'radio',
          '#suffix' => $op_suffix,
          '#prefix' => $op_prefix,
          '#attributes' => array('class' => 'signup-radio-button'),
          '#return_value' => $i,
          '#name' => 'signup_options',
          '#title' => t('Cancel'),
        );
      }
      elseif (isset($options[$i]) && (($options[$i]['slots'] == 0) || $options[$i]['slots'] - count($signees[$i]) > 0)) {
        $form['signup_options'][$i] = array(
          '#type' => 'radio',
          '#suffix' => $op_suffix,
          '#prefix' => $op_prefix,
          '#return_value' => $i,
          '#title' => t('Sign up'),
          '#name' => 'signup_options',
          '#attributes' => array('class' => 'signup-radio-button'),
        );
      }
      else {
        $form['signup_options'][$i] = array(
          '#type' => 'markup',
          '#suffix' => $op_suffix,
          '#prefix' => $op_prefix,
          '#value' => 'Registration closed.',
        );
      }
    }
  }
  $form['signup_options']['#validate'] = '_signup_options_signup_form_validate';
  $form['signup_options']['#submit'] = '_signup_options_signup_form_submit';
  return $form;
}

function _signup_options_signup_form_validate(&$form, &$form_state) {
  global $user;
  if (!user_access('sign up for content')) {
    form_set_error(NULL, t('You do not have permission to sign up for this content.'));
    return;
  }

  $slot = $form_state['values']['signup_options'];

  if (!$slot) {
    form_set_error('signup_options', t('Please select an option.'));
    return;
  }

  if (!empty($options) && (!isset($options[$slot]) || !($options[$slot]['slots'] == 0))) {

    $numreg = db_result(db_query("SELECT count(uid) FROM {signup_log} WHERE nid = %d AND oid = %d", array($node->nid, $slot)));
    
    if (!($options[$slot]['slots'] - $numreg > 0)) {
      form_set_error(NULL, t('Option is full. No more users can sign up'));
      $form['signup_options'][$slot]['#value'] = t('Registration closed');
      return;
    }
  }
}

function _signup_options_signup_form_submit($form, $form_state) {
  global $user;
  $nid = $form['nid']['#value'];

  $slot = $form_state['values']['signup_options'];
  if (!isset($slot)) {
    return;
  }
  if (in_array($form['signup_options'][$slot]['#title'], array(t('Sign up'), t('Cancel')))) {
    $signup = db_fetch_object(db_query('SELECT * FROM {signup_log} WHERE uid = %d AND nid = %d', array($user->uid, $nid)));
    if (!empty($signup)) {
      signup_cancel_signup($signup, FALSE);
    }
  }
  if ($form['signup_options'][$slot]['#title'] == t('Sign up')) {
    // Construct the appropriate $signup object representing this signup.
    $signup = new stdClass;
    $signup->nid = $nid;
    // Grab the values from the $account object we care about
    foreach (array('uid', 'name', 'mail') as $field) {
      $signup->$field = $user->$field;
    }

    // Other special signup values.
    $signup->signup_time = time();
    $signup->oid = "$slot";

    // Insert the signup into the {signup_log} table.
    signup_save_signup($signup);
  }
}


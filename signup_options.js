Drupal.behaviors.signup_options = function () {
    $('.signup-radio-button').hide();
    $('.signup-options-signup-submit').hide();
    $('.signup-options-column label.option').addClass('signup-options-fake-button');
    $('.signup-options-column label.option').mousedown( function() {
	$(this).toggleClass('signup-options-fake-button-down', true);
	$(this).toggleClass('signup-options-fake-button', false);
    });
    $('.signup-options-column label.option').mouseup( function() {
	$(this).toggleClass('signup-options-fake-button-down', false);
	$(this).toggleClass('signup-options-fake-button', true);
    });
    $('.signup-options-column label.option').click( function() {
	var forms = $(this).parents('form');
	$('input', this).attr("checked", "checked");
	$(forms[0]).submit();
    });
}